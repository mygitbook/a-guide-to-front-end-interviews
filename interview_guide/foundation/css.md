# CSS相关面试题

## CSS的BFC机制和触发元素

BFC（Block Formatting Context）是指在CSS中，一个独立的块级渲染环境，它决定了其子元素如何布局、定位和相互作用。

### BFC拥有一些特性：

1. 浮动元素会创建BFC，这意味着浮动元素的周围不会有其他元素。
2. 绝对定位元素会创建BFC，这意味着绝对定位元素的周围不会有其他元素。
3. 根元素是一个BFC。
4. 一些属性会触发元素的BFC特性，比如设置element的overflow为除了visible之外的值，会创建一个新的BFC。

### BFC的作用：

1. BFC可以阻止外部元素的浮动对内部元素的影响，避免元素被浮动元素覆盖。
2. BFC可以包含浮动元素，使父元素能够正确计算其高度。
3. 两个相邻的BFC之间的垂直边距会发生重叠，这可以用来实现垂直间距的效果。
4. BFC可以使元素自适应其包含块的宽度，防止元素溢出。

总结来说，BFC是一个独立的块级渲染环境，可以解决浮动、定位、清除浮动等布局问题，具有很重要的作用。

### CSS中可以触发BFC（块级格式化上下文）的元素有：

根元素（html）。 浮动元素（float不为none）。 绝对定位元素（position为absolute或fixed）。 行内块元素（display为inline-block）。 表格单元格（display为table-cell，table-caption，inline-table）。 表格标题（display为table-caption）。 包含表格的块元素（display为table）。 overflow属性值不为visible的块元素。 弹性盒子容器（display为flex或inline-flex）。
