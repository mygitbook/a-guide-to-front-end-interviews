---
description: TypeScript面试题更新一
---

# TypeScript

## 在TypeScript中tsconfig.json有什么作用？

tsconfig.json是TypeScript的配置文件，用于设置编译器的行为和选项。

在tsconfig.json中，可以配置以下选项来指定项目的编译规则和行为：

* compilerOptions：设置编译器的选项，例如编译目标版本、模块解析方式、输出目录等。
* include：指定要包含在编译中的文件或文件夹的路径模式。
* exclude：排除不需要编译的文件或文件夹的路径模式。
* files：指定要包含在编译中的具体文件路径列表。
* extends：继承其他配置文件，以便共享通用配置。

通过修改tsconfig.json文件中的配置，可以根据需要定制TypeScript项目的编译规则和行为，以便获得更好的开发体验和更高的代码质量。

## 在TypeScript中说说你对声明合并的理解？

在TypeScript中，声明合并是指当我们多次声明同一个名称的实体（函数、类、接口等）时，TypeScript会将这些声明合并为一个声明。这样可以更方便地在不同地方编写声明，同时也可以扩展和修改已有的声明。

声明合并的规则如下：

* 对于同一个名称的函数，会将它们的参数类型合并为一个重载列表，然后将返回类型合并为联合类型。
* 对于同一个名称的命名空间，会将它们的成员合并为一个命名空间（模块）。
* 对于同一个名称的接口，会将它们的成员合并为一个接口，接口的成员列表会合并在一起。
* 对于同一个名称的类，会将它们的成员合并为一个类，类的成员列表会合并在一起，静态成员和实例成员分开处理。

通过声明合并，我们可以更轻松地扩展现有的类型声明，而不需要修改原有的代码。这在使用第三方库时特别有用，可以避免直接修改库的类型声明文件。此外，可以通过声明合并为现有的类型添加新的属性和方法，以适应特定的业务需求。但需要注意的是，声明合并并不会对实际的运行时代码产生影响，它只是在编译阶段起作用。

## 在TypeScript中as语法是什么？

在TypeScript中，as语法是一种类型断言语法。它允许开发者在某个表达式上指定一个特定的类型，即强制类型转换。

语法格式如下：

```typescript
expression as type
```

as语法被用于类型断言，表明开发者的确知道某个值的类型，并希望编译器按照指定的类型进行处理，从而避免编译器的类型检查报错。

例如，以JavaScript中的NaN特殊值为例，当使用+运算符对NaN进行运算时，结果仍为NaN。但在TypeScript中，如果对一个unknown类型的值进行加法运算，编译器会报错。这时，可以使用as语法断言将该值转换为number类型，如下所示：

```typescript
let value: unknown = NaN;
let result = value as number + 5;
console.log(result);  // 输出 NaN
```

在这个例子中，由于value的类型是unknown，编译器会报错。通过使用as语法将value断言为number类型，就能解决这个问题，并且在执行加法运算时，结果仍然是NaN。

## 在TypeScript中枚举和常量枚举有什么区别？

在TypeScript中，枚举是一种特殊的数据类型，用来定义一组命名的常量。常量枚举是指在编译阶段被删除并且只能在编译阶段使用的枚举。

区别如下：

* 枚举会在运行时存在，可以在运行时使用，而常量枚举在编译后会被删除，只能在编译阶段使用。
* 枚举成员的值可以是字符串、数字或计算过的值，而常量枚举的成员只能是字符串或数字。
* 枚举成员的值可以是动态的，而常量枚举的成员的值必须是编译时可确定的常量表达式。
* 枚举成员可以有方法，而常量枚举的成员不能有方法。
* 使用枚举成员需要通过枚举名访问，而使用常量枚举成员可以直接通过成员名访问。

## 在TypeScript中如何实现继承？

在 TypeScript 中，可以通过关键字 `extends` 来实现类的继承。子类可以继承父类的属性和方法，并可以添加自己的属性和方法。以下是一个示例：

```typescript
class Animal {
  name: string;

  constructor(name: string) {
    this.name = name;
  }

  sayHello() {
    console.log(`Hello, my name is ${this.name}`);
  }
}

class Dog extends Animal {
  breed: string;

  constructor(name: string, breed: string) {
    super(name);
    this.breed = breed;
  }

  sayBreed() {
    console.log(`My breed is ${this.breed}`);
  }
}

const myDog = new Dog('Buddy', 'Labrador');
myDog.sayHello();
myDog.sayBreed();
```

在上面的例子中，`Animal` 类是一个基类，`Dog` 类是一个派生类。`Dog` 类通过 `extends` 关键字继承 `Animal` 类，从而获得了 `name` 属性和 `sayHello` 方法。`Dog` 类还定义了自己的 `breed` 属性和 `sayBreed` 方法。可以通过创建 `Dog` 的实例来调用继承自 `Animal` 的方法和自己定义的方法。

## 如何将多个ts文件合并为一个js文件？

要将多个ts文件合并为一个js文件，可以使用以下步骤：

1. 确保你的ts文件都在同一个文件夹中。
2. 打开终端或命令行界面，进入该文件夹。
3.  执行以下命令来安装typescript编译器：

    ```shell
    npm install -g typescript
    ```
4.  执行以下命令来编译ts文件并生成对应的js文件：

    ```shell
    tsc file1.ts file2.ts file3.ts
    ```

    这将编译文件1、文件2和文件3，并生成对应的js文件。
5.  如果你想将所有的ts文件合并为一个js文件，可以在终端中执行以下命令：

    ```bash
    tsc --outFile bundle.js file1.ts file2.ts file3.ts
    ```

    这将把所有的ts文件合并为一个名为bundle.js的js文件。
6. 现在，你就可以在该文件夹中找到生成的bundle.js文件，它包含了所有合并的ts文件的代码。

注意：在执行上述命令之前，你需要先确保已经安装了Node.js和TypeScript编译器。

## 在TypeScript支持静态类吗？为什么？

TypeScript不直接支持静态类。静态类是指只包含静态成员（属性和方法）的类，不需要实例化即可访问。

TypeScript鼓励使用命名空间和模块来组织代码，而不是通过静态类来定义全局的静态成员。可以将静态成员放在命名空间或模块的顶级，以便在需要时直接访问。

尽管如此，你仍然可以在TypeScript中实现静态类的行为，通过使用Constructor函数和静态属性和方法的组合来实现。下面是一个示例：

```typescript
class StaticClass {
  static staticProperty: number = 0;
  
  static staticMethod(): void {
    console.log("Hello, I'm a static method!");
  }
}

// Access static property
console.log(StaticClass.staticProperty);

// Call static method
StaticClass.staticMethod();
```

虽然这样可以实现类似于静态类的行为，但是请务必谨慎使用，因为这样容易导致代码的可维护性和可测试性下降。尽量遵循TypeScript的设计原则和最佳实践，使用命名空间和模块来组织代码，以提高代码的可读性和可维护性。

## 在TypeScript中控制成员可见性有哪些方法？

在TypeScript中，可以使用访问修饰符来控制成员的可见性。访问修饰符有以下几种：

1. public(默认)：成员可以被自身类、子类和外部访问。
2. private：成员只能被自身类访问。
3. protected：成员可以被自身类和子类访问，但不能被外部访问。
4. readonly：只读修饰符，只能在声明时或构造函数内赋值。

对于类中的成员（属性和方法），可以使用这些访问修饰符来限制其可见性。例如：

```typescript
class Person {
  private name: string;
  public age: number;
  protected address: string;
  readonly id: string;

  constructor(name: string, age: number, address: string, id: string) {
    this.name = name;
    this.age = age;
    this.address = address;
    this.id = id;
  }

  private getName(): string {
    return this.name;
  }

  public getAge(): number {
    return this.age;
  }

  protected getAddress(): string {
    return this.address;
  }

  readonly getId(): string {
    return this.id;
  }
}

class Employee extends Person {
  private department: string;

  constructor(name: string, age: number, address: string, id: string, department: string) {
    super(name, age, address, id);
    this.department = department;
  }

  getDepartment(): string {
    return this.department;
  }
}

const person = new Person("John Doe", 30, "123 Main St", "12345");
console.log(person.age); // 可以访问public属性
console.log(person.address); // 错误，无法访问protected属性
console.log(person.id); // 可以访问readonly属性

const employee = new Employee("Jane Smith", 25, "789 Oak St", "54321", "HR");
console.log(employee.getDepartment()); 
// 可以访问子类中的公共方法
console.log(employee.address); 
// 错误，无法访问受保护的属性
```

## 在TypeScript中如何创建对象？

在TypeScript中，可以使用类来创建对象。

首先，需要定义一个类，然后使用`new`关键字实例化该类，就可以创建一个对象。

例如，我们有一个名为`Person`的类：

```typescript
class Person {
  firstName: string;
  lastName: string;

  constructor(firstName: string, lastName: string) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  getFullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}
```

然后，我们可以使用该类来创建一个`Person`对象：

```typescript
const person = new Person('John', 'Doe');
```

现在，`person`就是一个`Person`对象。我们可以访问该对象的属性和方法，例如：

```typescript
console.log(person.firstName); // 输出：John
console.log(person.lastName);  // 输出：Doe

console.log(person.getFullName()); // 输出：John Doe
```

## 在TypeScript中如何定义和使用泛型？

在TypeScript中，我们可以使用泛型来创建通用的函数、类和接口，从而实现代码的复用性和灵活性。

首先，我们使用`<>`来声明泛型。在`<>`中可以定义一个或多个类型参数，用于表示函数、类或接口中的某个未知的类型。

下面是一个简单的例子，演示了如何定义和使用泛型函数：

```typescript
function identity<T>(arg: T): T {
  return arg;
}

let output = identity<string>("hello");
console.log(output);  // 输出 "hello"

output = identity(42);
console.log(output);  // 输出 42
```

在上面的例子中，`identity`函数使用了泛型参数`<T>`来表示一个未知的类型。这样，我们可以将函数应用于多种类型的参数，并返回相同类型的值。

我们还可以使用泛型来定义泛型类和泛型接口。下面是一个泛型类的例子：

```typescript
class Queue<T> {
  private items: T[] = [];

  enqueue(item: T) {
    this.items.push(item);
  }

  dequeue(): T {
    return this.items.shift();
  }
}

let queue = new Queue<number>();
queue.enqueue(1);
queue.enqueue(2);
console.log(queue.dequeue());  // 输出 1
console.log(queue.dequeue());  // 输出 2
```

在上面的例子中，`Queue`类使用了泛型参数`<T>`来表示队列中的元素的类型。这样，我们可以创建多种类型的队列，例如`Queue<number>`表示一个整数队列，`Queue<string>`表示一个字符串队列。

最后，我们还可以使用泛型约束来限制泛型的类型。例如，我们可以使用`extends`关键字来指定泛型的上界，要求泛型类型满足指定的条件。

下面是一个使用泛型约束的例子：

```typescript
interface Lengthwise {
  length: number;
}

function loggingIdentity<T extends Lengthwise>(arg: T): T {
  console.log(arg.length);
  return arg;
}

loggingIdentity("hello");      // 输出 5
loggingIdentity([1, 2, 3]);    // 输出 3
```

在上面的例子中，`Lengthwise`接口定义了一个包含`length`属性的类型。`loggingIdentity`函数使

## 什么是TypeScript接口？

TypeScript接口是一种用于定义对象的结构和行为的方式。它类似于Java和C#中的接口， 可以描述对象的属性和方法，并且可以用于类型检查和类型推断。使用接口可以使你的代码更加模块化、灵活和可复用。

## 在TypeScript中如何与其他工具和库集成？

在TypeScript中可以与其他工具和库集成的方式有很多，以下是一些常见的集成方式：

1. 使用模块管理器：可以使用像Webpack、Parcel、Rollup等模块管理器来打包和构建TypeScript项目。这些工具可以帮助你处理TypeScript文件的依赖关系，并生成用于浏览器或Node.js的最终输出。
2. 使用类型声明文件：TypeScript与JavaScript的生态系统有很好的互动性。你可以使用npm或yarn来安装JavaScript库，并使用它们的类型声明文件（通常以`.d.ts`文件为扩展名）来为TypeScript项目添加类型支持。类型声明文件描述了库的API和类型信息，让TypeScript编译器能够提供代码补全、错误检查和类型推导等功能。
3. 使用编辑器插件：TypeScript有很多与编辑器（如VS Code、Sublime Text、Atom等）集成的插件。这些插件可以提供更好的语法高亮、代码补全、错误检查、重构等功能。例如，VS Code提供了`typescript-language-service`插件，可以为TypeScript项目提供实时代码补全和错误检查。
4. 使用构建工具：除了模块管理器外，还可以使用构建工具来构建和部署TypeScript项目。例如，你可以使用像gulp、grunt等构建工具来自动执行一系列任务，例如编译TypeScript、压缩代码、拷贝文件等。
5. 使用测试框架：TypeScript可以与许多测试框架集成，用于编写单元测试、集成测试等。常见的测试框架包括Jest、Mocha、Chai等。你可以使用这些框架来编写TypeScript测试，并运行它们以验证代码的正确性。

总而言之，TypeScript可以与许多工具和库集成，以获得更好的开发体验和更高的生产力。你可以选择适合自己项目的集成方式，并根据需要进行配置和定制。

## 在TypeScript中的lambda函数是什么？

在TypeScript中，Lambda函数又被称为箭头函数（Arrow Function）。它是一种更简洁的函数语法形式，可以用来定义匿名函数。

Lambda函数的语法如下：

```typescript
(param1: type, param2: type, ...) => { statements }
```

其中，()括号中是函数的参数列表，=>是箭头符号，{}括号中是函数体，statements表示函数的执行语句。

Lambda函数的特点是可以自动绑定函数体中的this指针，并且可以省略function关键字和return关键字。另外，如果函数体只包含一条语句，还可以省略{}括号和分号，直接写在=>后面。

下面是一个使用Lambda函数的示例：

```typescript
let add = (a: number, b: number) => a + b;
console.log(add(1, 2)); // 输出3
```

在上面的示例中，我们使用Lambda函数定义了一个名为add的函数，它接受两个参数a和b，并返回它们的和。然后，在后面的代码中，我们调用add函数，并传入参数1和2，输出结果为3。

## 在Typescript中如何将字符串转换为数字？

在TypeScript中，可以使用`parseInt()`或`parseFloat()`函数将字符串转换为数字。

1. 使用`parseInt()`函数将字符串转换为整数。

```typescript
let str: string = "10";
let num: number = parseInt(str);
console.log(num); // 10
```

2. 使用`parseFloat()`函数将字符串转换为浮点数。

```typescript
let str: string = "10.5";
let num: number = parseFloat(str);
console.log(num); // 10.5
```

请注意，如果字符串无法转换为数字，`parseInt()`和`parseFloat()`函数将返回`NaN`（Not a Number）。因此，在使用转换后的数字之前，要进行适当的错误检查。

```typescript
let str: string = "abc";
let num: number = parseInt(str);
if (isNaN(num)) {
  console.log("Invalid number");
} else {
  console.log(num);
}
```

## 在TypeScript中如何访问模块外定义的类？

要在TypeScript中访问模块外定义的类，可以使用命名空间（namespace）的方式。

首先，在类定义所在的文件中，将该类放在一个命名空间中。例如，定义一个名为"myModule"的命名空间，并在其中定义一个类"MyClass"，代码如下：

```typescript
namespace myModule {
  export class MyClass {
    // class definition
  }
}
```

然后，在需要使用该类的文件中，通过`import`语句引入该命名空间，并使用命名空间和类名来访问该类。例如：

```typescript
import { myModule } from './path/to/myModule';

let instance = new myModule.MyClass();
```

在上面的例子中，引入命名空间时使用相对路径`'./path/to/myModule'`，你需要根据实际的路径进行调整。然后，可以通过在命名空间和类名之间加上`.`来访问类。在这个例子中，我们创建了一个类的实例`instance`。

另外需要注意的是，为了在模块外部访问命名空间中的类，需要在类的定义前面加上`export`关键字，将类导出为一个可访问的模块成员。这样，在引入命名空间时，就能够访问到这个类了。

## 在TypeScript中的getter/setter是什么？有什么作用？

在TypeScript中，getter和setter是用于访问和修改类中私有属性的特殊方法。getter用于获取属性的值，setter用于设置属性的值。

getter和setter的作用有以下几点：

1. 封装属性：getter和setter提供了一种封装属性的方式，通过定义getter和setter方法来控制对属性的访问和修改。这样可以更好地管理属性的访问权限，并且可以在访问或修改属性时进行验证或其他逻辑操作。
2. 计算属性：通过getter方法，可以根据其他属性的值计算并返回一个新的属性值。这样可以简化属性的计算逻辑，使代码更清晰易读。
3. 与JavaScript兼容：getter和setter在TypeScript中的语法与JavaScript中的属性访问器语法类似，因此可以轻松地与现有的JavaScript代码和库集成。

下面是一个使用getter和setter的示例：

```typescript
class Circle {
  private _radius: number;

  get radius(): number {
    return this._radius;
  }

  set radius(value: number) {
    if (value < 0) {
      throw new Error('Radius cannot be negative');
    }
    this._radius = value;
  }

  get area(): number {
    return Math.PI * this._radius * this._radius;
  }
}

const circle = new Circle();
circle.radius = 5;
console.log(circle.radius); // Output: 5
console.log(circle.area); // Output: 78.53981633974483
```

在上面的示例中，`Circle`类定义了一个私有属性`_radius`，并使用getter和setter方法`radius`来访问和修改这个属性。`area`属性则是一个计算属性，根据`_radius`计算出圆的面积。使用getter和setter，可以对半径的值进行验证

## 在TypeScript中支持哪些JSX模式？

在TypeScript中，支持两种JSX模式：

1. React JSX模式：这是最常见的模式，用于React项目中。在这种模式下，可以使用JSX语法来编写组件模板，并使用TypeScript类型检查和提示。

```tsx
import React from 'react';

type Props = {
  name: string;
};

const MyComponent = (props: Props) => {
  return <div>Hello, {props.name}!</div>;
};
```

2. TypeScript JSX模式：这种模式是为了支持非React项目中使用JSX语法而设计的。在这种模式下，可以使用JSX语法来编写任何类型的组件模板，而不仅仅是React组件。

```tsx
type Props = {
  name: string;
};

const MyComponent = (props: Props) => {
  return <div>Hello, {props.name}!</div>;
};
```

要在TypeScript中启用JSX支持，需要在`tsconfig.json`文件中设置`"jsx"`选项为`"react"`或`"preserve"`。

```json
{
  "compilerOptions": {
    "jsx": "react"
  }
}
```

在React项目中使用React JSX模式，而在非React项目中使用TypeScript JSX模式。

## 在TypeScript中如何让接口的所有属性都可选？

在TypeScript中，可以使用Partial预定义类型将接口的所有属性都标记为可选。以下是一个示例：

```typescript
interface Person {
  name: string;
  age: number;
  gender: string;
}

// 将Person接口的所有属性都标记为可选
type OptionalPerson = Partial<Person>;

// 使用OptionalPerson类型
const person: OptionalPerson = {
  name: 'John',
  age: 25
};
```

在上面的示例中，我们首先定义了一个名为Person的接口，它具有三个属性：name、age和gender。然后，使用Partial创建了一个名为OptionalPerson的类型，将Person接口的所有属性都标记为可选。最后，我们声明了一个使用OptionalPerson类型的变量person，其中只给了name和age属性的值，而gender属性则留空。

## 在TypeScript中如何从子类调用基类构造函数？

在TypeScript中，可以使用`super()`关键字来调用父类的构造函数。在子类的构造函数中，可以使用`super()`来调用父类的构造函数，并传递参数。

以下是一个示例代码：

```typescript
class Person {
  name: string;

  constructor(name: string) {
    this.name = name;
  }

  sayHello() {
    console.log(`Hello, ${this.name}!`);
  }
}

class Student extends Person {
  grade: number;

  constructor(name: string, grade: number) {
    super(name);
    this.grade = grade;
  }

  study() {
    console.log(`${this.name} is studying in grade ${this.grade}.`);
  }
}

const student = new Student('Alice', 10);
student.sayHello();  // 输出：Hello, Alice!
student.study();  // 输出：Alice is studying in grade 10.
```

在上面的示例中，`Student`类是`Person`类的子类，它调用了父类`Person`的构造函数并传递了名字参数。然后，`Student`类自己有一个额外的属性`grade`，并定义了一个自己的方法`study()`。

在创建`Student`实例时，可以看到它同时具有`Person`类和`Student`类的属性和方法，并且可以正常使用。

## 在TypeScript中有哪些三斜杠指令？

在TypeScript中，有三个三斜杠指令可以使用，它们是：

1. /// ：用于引用其他的代码文件，类似于JavaScript中的`<script>`标签。该指令告诉编译器在编译时需要合并指定路径的文件。
2. /// ：用于引用第三方类型声明文件。该指令告诉编译器使用指定的类型声明文件。
3. /// ：用于引用特定的内置库文件。该指令告诉编译器使用指定的内置库文件，例如`"es6"`表示使用ES6标准库文件。

## 在TypeScript中是如何检查null和undefined的？

在TypeScript中，可以使用非空断言`!`来告诉编译器一个表达式的值不会是null或undefined。例如，如果你确定一个变量不会是null或undefined，你可以使用非空断言来告诉编译器不要对它进行严格的空值检查。

另外，TypeScript还提供了一些类型保护机制来检查null和undefined。通过使用类型守卫，可以在代码中检查类型，并根据类型来执行不同的操作。

以下是一些常见的用例：

1. 可选链操作符：TypeScript 3.7及以上版本引入的新特性，可使用可选链操作符`?.`来访问一个可能为null或undefined的属性或方法，如果对象不存在，则返回undefined。

```typescript
const length = obj?.property?.length;
```

2. 类型保护：可以使用类型保护来检查一个值是否为null或undefined，并在代码中做出相应的处理。

```typescript
if (obj !== null && obj !== undefined) {
  // do something with obj
}

// 或者

if (obj != null) {
  // do something with obj
}
```

3. 非空断言：可以使用非空断言`!`来告诉编译器一个表达式的值不会是null或undefined。

```typescript
const length = obj!.property.length;
```

请注意，非空断言应该谨慎使用，因为它会绕过TypeScript的类型检查机制，如果使用不当，可能导致运行时错误。在使用非空断言时，确保你对值的可用性有一个明确的了解。

## 在TypeScript中如何创建变量？

在TypeScript中，可以使用关键字`let`或`const`来创建变量。

如果变量的值会被修改，可以使用`let`关键字来声明变量，并初始化一个值。例如：

```typescript
let age: number = 18;
```

如果变量的值不会被修改，可以使用`const`关键字来声明常量。例如：

```typescript
const name: string = "John";
```

可以在声明变量的同时给它一个初始值，也可以在后续的代码中给变量赋值。例如：

```typescript
let x: number = 10;
x = 20;
```

需要注意的是，在TypeScript中可以对`let`变量进行重新赋值，但对`const`常量进行重新赋值会引发编译错误。因此，应该尽量使用`const`来声明不会被修改的值。

## 在TypeScript中解释下rest参数的作用及规则

在TypeScript中，rest参数用来表示函数中接收多个同类型的参数。它使用三个连续的点(...)作为前缀，并跟上一个参数名，表示这个参数将会接收任意数量的该类型的参数，并将它们放入一个数组中。

rest参数的规则如下：

1. rest参数必须是函数的最后一个参数，否则会报错。
2. rest参数的类型是一个数组，数组元素的类型就是指定的参数类型。
3. rest参数可以有0个或多个参数，也可以是可选的（使用问号?进行标记）。
4. rest参数前可以有其他参数，但它们将不会被包含在rest参数的数组中。

下面是一个使用rest参数的示例：

```typescript
function sum(...numbers: number[]) {
  let total = 0;
  for (let num of numbers) {
    total += num;
  }
  return total;
}

console.log(sum(1, 2, 3)); // 6
console.log(sum(1, 2, 3, 4, 5)); // 15
console.log(sum()); // 0
```

在上面的例子中，`sum`函数定义了一个rest参数`numbers`，它可以接收任意数量的`number`类型的参数。在函数体内部，我们通过遍历`numbers`数组来计算所有参数的总和。我们可以看到，调用`sum`函数时我们可以传入任意数量的参数，函数会自动将它们放入一个数组中。

## 你是如何编译TypeScript的文件？

要编译TypeScript文件，你可以使用TypeScript编译器（tsc）命令行工具。

以下是一些编译TypeScript文件的常见方法：

1. 手动编译：在命令行中，使用`tsc`命令后跟文件路径来手动编译单个TypeScript文件。例如：`tsc path/to/file.ts`。
2. 批量编译：要编译多个TypeScript文件，可以在命令行中使用通配符（\*）来指定要编译的文件。例如：`tsc *.ts`。
3. 监视模式：使用`tsc`命令的`--watch`参数可以启用监视模式，以实时监视TypeScript文件的更改并自动重新编译。例如：`tsc --watch file.ts`。
4. 配置文件：你还可以使用TypeScript配置文件（tsconfig.json）来配置编译选项。在项目根目录中创建一个名为`tsconfig.json`的文件，并在其中指定编译选项。然后，在命令行中使用`tsc`命令来编译整个项目或使用`--project`参数来指定配置文件路径。例如：`tsc`（在项目根目录中运行）或 `tsc --project tsconfig.json`。

这些是一些常用的编译TypeScript文件的方法。你可以根据需要选择适合你的方法。

## 请说说在TypeScript中什么是联合类型？

在TypeScript中，联合类型表示一个变量可以是多种不同类型之一。它使用竖线（|）分隔每个可能的类型。当我们不确定一个变量的确切类型时，可以使用联合类型。联合类型可以用于参数、返回值、变量等不同的地方。

例如，下面的代码定义了一个变量`myVar`，它可以是字符串类型或者数字类型：

```typescript
let myVar: string | number;

myVar = "Hello";
console.log(myVar.length); // 输出 5

myVar = 123;
console.log(myVar.toFixed(2)); // 输出 123.00
```

在上面的例子中，变量`myVar`可以是字符串类型或者数字类型，根据当前赋值的类型不同，可以调用不同类型的方法或者属性。通过联合类型，我们可以更灵活地处理不同类型的变量。

## 在TypeScript中是如何实现函数重载的？

在TypeScript中，函数重载允许我们为同一个函数提供多个函数类型定义。这样在调用函数时会根据传入的参数类型选择合适的函数类型定义来执行。我们可以使用`function`关键字定义函数，并使用重载声明来定义多个函数类型定义。

下面是一个示例：

```typescript
function calculateArea(length: number): number;
function calculateArea(length: number, width: number): number;

function calculateArea(length: number, width?: number): number {
  if (width) {
    return length * width;
  }
  return length * length;
}

let area1 = calculateArea(10); // 调用第一个函数类型定义
let area2 = calculateArea(10, 20); // 调用第二个函数类型定义
```

在上面的示例中，我们定义了一个函数`calculateArea`，它接收一个参数`length`和一个可选参数`width`。我们使用重载声明`function calculateArea(length: number): number;`和`function calculateArea(length: number, width: number): number;`来定义两个函数类型定义，分别对应只传入`length`参数和同时传入`length`和`width`参数的情况。

在具体实现时，我们使用了一个函数`calculateArea(length: number, width?: number): number`，它根据`width`参数是否存在来决定返回不同的计算结果。

当我们调用`calculateArea`函数时，TypeScript会自动根据传入的参数类型选择合适的函数类型定义来执行。通过使用函数重载，我们可以提高代码的可读性和可维护性。

## 在TypeScript中使用unknown的场景是什么？

在TypeScript中，使用`unknown`类型可以表示一个不确定的值的类型。这个类型类似于`any`类型，但是`unknown`类型有一些限制。

使用`unknown`的场景包括：

1. 当无法确定某个变量的类型时，可以使用`unknown`来防止错误的使用。
2. 在使用动态类型的库或框架时，可以将返回值的类型设置为`unknown`，然后使用类型断言或类型细化来指定具体的类型。
3. 当需要通过类型检查来确保安全性时，可以使用`unknown`来替代`any`类型。因为`unknown`类型可以强制使用类型断言来明确类型，从而减少潜在的错误。

## 在TypeScript中如何定义类？

在TypeScript中，可以使用`class`关键字定义一个类。类可以包含属性、方法和构造函数。

例如，下面是一个定义了一个名为`Person`的类的例子：

```typescript
class Person {
  name: string;
  age: number;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }

  sayHello() {
    console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`);
  }
}

const john = new Person("John", 30);
john.sayHello();  // 输出 "Hello, my name is John and I am 30 years old."
```

在这个例子中，`Person`类有两个属性：`name`和`age`，以及一个构造函数和一个`sayHello`方法。创建一个`Person`对象后，可以通过调用`sayHello`方法来输出对象的属性值。

除了普通属性和方法，类还可以具有静态属性和静态方法。静态属性和方法可以在类的实例化之前直接访问。

下面是一个包含静态属性和方法的类的例子：

```typescript
class MathUtils {
  static PI: number = 3.14159;

  static calculateArea(radius: number) {
    return MathUtils.PI * radius * radius;
  }
}

console.log(MathUtils.calculateArea(5));  // 输出 "78.53975"
```

在这个例子中，`MathUtils`类具有一个静态属性`PI`和一个静态方法`calculateArea`。可以通过类名直接访问这些静态属性和方法，而不需要创建实例。

## 在TypeScript中的类与JavaScript类有什么不同？

在TypeScript中的类与JavaScript类的主要区别在于类型系统的支持。TypeScript是一种类型安全的超集，它允许开发者在类中指定属性和方法的类型，并且可以进行类型检查和类型推断。这样就可以在开发过程中捕捉到一些潜在的错误，并提供更好的代码智能提示。

除了类型支持外，TypeScript还提供了一些其他的特性和语法糖，例如修饰器、泛型等，使得类的定义和使用更加灵活和强大。此外，TypeScript还支持ES6中的类继承、接口实现、访问修饰符等特性。

因此，使用TypeScript编写类可以提供更好的代码可维护性和可读性，并且可以在开发阶段捕捉到一些常见的错误，从而提升开发效率和代码质量。

## 在TypeScript中是怎样进行类型检查的？

在TypeScript中，类型检查是通过静态类型检查器来实现的。静态类型检查器会分析代码中的类型注解和类型推断，并根据这些信息来验证代码的正确性。

类型注解是通过在代码中显式地指定类型来进行类型检查的一种方式。例如，在变量声明时使用冒号来指定类型：

```typescript
let name: string = "John";
```

在上面的例子中，`name` 的类型被注解为 `string`，这样静态类型检查器就会验证赋给 `name` 的值是否为字符串类型。

另一种方式是通过类型推断来进行类型检查。当代码中没有显式指定类型注解时，静态类型检查器会根据代码的上下文自动推断出变量的类型。例如：

```typescript
let age = 28;
```

在上面的例子中，`age` 的类型会被推断为 `number`，因为赋给它的值是一个数字。

静态类型检查器会对代码进行静态分析，检查类型的一致性和错误。它会查找类型错误，例如将不同类型的值赋给变量，或者在不支持的操作中使用不兼容的类型。如果代码存在类型错误，就会给出相应的类型错误提示。

总结来说，在TypeScript中，类型检查是通过类型注解和类型推断来实现的，通过静态类型检查器对代码进行静态分析并进行类型验证。这样能够在编译阶段捕获潜在的类型错误，以提高代码的稳定性和可靠性。

## 在TypeScript中的void类型是什么？在什么场景下使用？

在TypeScript中，void类型表示函数没有返回值。它可以用作函数的返回类型，表示函数不会返回任何值。 一个典型的场景是对于没有返回值的函数，比如只负责打印一些信息、执行一些操作等，而不需要返回任何值。下面是一个示例：

```typescript
function sayHello(): void {
  console.log("Hello, world!");
}

sayHello(); // 输出 "Hello, world!"
```

在这个例子中，`sayHello`函数的返回类型被声明为`void`，并且函数体内只包含一个打印语句。这意味着该函数不会返回任何值，而只是打印一条信息。

## 在TypeScript中如何支持类型推断？

TypeScript支持类型推断，它可以根据变量的初始值自动推断其类型。以下是一些使用类型推断的示例：

```typescript
// 推断为字符串类型
let message = "hello";

// 推断为数字类型
let count = 3;

// 推断为布尔类型
let isTrue = true;

// 推断为数组类型
let numbers = [1, 2, 3];

// 推断为对象类型
let person = { name: "Alice", age: 30 };
```

此外，TypeScript还支持通过类型注解来显式地指定变量的类型，如下所示：

```typescript
let message: string = "hello";
let count: number = 3;
let isTrue: boolean = true;
let numbers: number[] = [1, 2, 3];
let person: { name: string, age: number } = { name: "Alice", age: 30 };
```

类型注解可以提供额外的类型检查和文档，但通常情况下，由于类型推断的存在，类型注解并不是必需的。

## 在TypeScript中any和unkown有什么区别？

在TypeScript中，`any`和`unknown`都是表示不确定类型的关键字。

1. `any`表示任意类型，编译器不会对使用`any`类型的值进行类型检查，可以进行任意操作。这意味着`any`类型可以包括任何类型的值，并且对任何类型的值都是兼容的。举例来说，如果一个变量声明为`any`类型，那么可以赋予任何类型的值给这个变量，而且编译器不会报错。

```typescript
let value: any;
value = 123; // okay
value = 'hello'; // okay
value = true; // okay
```

2. `unknown`表示未知类型，编译器要求使用`unknown`类型的值进行类型检查或者类型断言之后才能进行操作。这意味着`unknown`类型可以包含任何类型的值，但不能对其进行任意操作。对于使用`unknown`类型的变量，必须在使用之前进行类型检查或者类型断言来缩小类型范围。

```typescript
let value: unknown;
value = 123; // okay
value = 'hello'; // okay
value = true; // okay

console.log(value.toFixed()); // Error: Object is of type 'unknown'
console.log((value as number).toFixed()); // okay
```

因此，与`any`相比，`unknown`类型更安全，因为它要求进行类型检查或者类型断言才能进行操作。而`any`类型则更灵活，因为它允许进行任意操作，但同时也带来了类型安全性的风险。在实际使用中，应尽量避免使用`any`类型，而是优先使用更具体的类型，同时慎用`unknown`类型，避免潜在的类型错误。

## 在TypeScript中装饰器有哪些应用场景？

在TypeScript中，装饰器有很多应用场景，以下是一些常见的例子：

1. 类装饰器：可以用来扩展类的功能，比如添加日志、性能统计等功能。
2. 方法装饰器：可以用来修改方法的行为，比如添加验证、缓存等功能。
3. 属性装饰器：可以用来修改属性的行为，比如添加默认值、数据校验等功能。
4. 参数装饰器：可以用来修改方法的参数，比如在参数上添加类型检查、权限校验等功能。

装饰器可以应用于类、方法、属性和参数等各种元素，通过装饰器可以在不修改源代码的情况下，给这些元素添加额外的功能。装饰器是TypeScript的特性，也是它的一个强大的特性，可以大大提升代码的可维护性和可扩展性。

## 请说说你对TypeScript中装饰器的理解

装饰器是TypeScript中的一种特殊语法，用于装饰、修改或扩展类、方法、属性或参数的行为。它可以在不修改目标代码的情况下，为目标代码添加额外的功能或元数据。

装饰器可以应用于类、方法、属性和参数上。通过在目标代码前面使用`@`符号，并且紧跟着一个装饰器函数，就可以将装饰器应用到目标代码上。

装饰器函数是一个普通的函数，它可以接收参数和返回值。参数可以是目标代码本身，也可以是其他装饰器相关的信息。返回值可以是一个新的目标代码，也可以是对原目标代码的修改。

装饰器在TypeScript中的应用非常广泛，可以用于实现诸如日志记录、鉴权、缓存、性能监控等功能。它可以使代码更加灵活、可复用和易于维护，同时也能提高开发效率。

## 请说说你对TypeScript中mixin的理解

在TypeScript中，mixin是一种将多个类的功能组合在一起的技术。通过使用mixin，我们可以将多个类中的成员（属性和方法）合并到一个新的类中，从而实现代码的复用和增强。

使用mixin可以实现多继承的效果，同时避免了多继承带来的问题（如菱形继承问题）。

在TypeScript中，我们可以通过以下步骤实现mixin：

1. 创建一个接口，用于标识混入类的结构。
2. 创建一个mixin函数，用于接受待混入的类和接口，并返回一个新的类，该类包含了传入类和接口中的成员。
3. 使用mixin函数将待混入的类和接口组合成一个新的类。

使用mixin可以实现一些常见的功能，例如给类添加日志、缓存、事件等功能，同时保持代码的可读性和可维护性。

需要注意的是，虽然TypeScript中没有直接支持mixin的语法，但通过借助装饰器、高阶函数等特性，我们可以很方便地实现mixin的功能。

## 请说说你对TypeScript中类的理解

在TypeScript中，类是一种面向对象的编程方式，用于描述具有相同属性和行为的对象的模板。类具有属性和方法，可以用来创建对象。

在TypeScript中，类可以包含私有属性和方法，以及公共属性和方法。私有属性和方法只能在类的内部访问，而公共属性和方法可以在类的内部和外部访问。

类还可以包括构造函数，用于初始化对象的属性。构造函数在创建对象时被调用，并可以接受参数来设置对象的初始状态。

类可以继承其他类，并扩展其属性和方法。继承可以通过关键字`extends`实现，并可以使用关键字`super`调用父类的构造函数和方法。

通过使用类，可以更好地组织和管理代码，并实现代码的复用和封装。在TypeScript中，类是一种重要的语言特性，它允许开发者以面向对象的方式构建复杂的应用程序。

## 在TypeScript中never与void有什么区别？

在TypeScript中，`never`和`void`是两个不同的类型。

`void`类型表示函数没有返回值的情况。当一个函数声明的返回类型为`void`时，这个函数可以不返回任何值或者返回`undefined`。

```typescript
function greeting(): void {
  console.log("Hello!");
}
```

`never`类型表示函数无法正常返回的情况。通常情况下，`never`类型用于表示函数抛出异常或者永远不会结束的循环。

```typescript
function throwError(message: string): never {
  throw new Error(message);
}

function endlessLoop(): never {
  while (true) {
   // do something
  }
}
```

总结一下，`void`类型表示函数没有返回值，而`never`类型表示函数无法正常返回。

## TypeScript有哪些内置数据类型？

TypeScript有以下内置数据类型：

* boolean：布尔值，表示true或false。
* number：数字，包括整型和浮点型。
* string：字符串。
* array：数组，可以包含任意类型的元素。
* tuple：元组，表示一个固定长度和类型的数组。
* enum：枚举，表示一组命名的常量。
* any：任意类型，可以为任意值。
* void：空类型，表示没有返回值的函数。
* null：表示null值。
* undefined：表示未定义的值。
* never：表示不可能发生的值的类型。

## 在TypeScript中interface与type有什么区别？

在TypeScript中，interface和type都是用来定义数据结构的。

interface主要用于定义对象的结构，它可以包含属性、方法和索引签名，并且可以被类或对象实现（implement）或继承（extend）。

type是用来定义特定的数据类型。它可以定义基本类型、联合类型、交叉类型等，还可以定义别名（type alias）。

区别如下：

* interface可以被类或对象实现或继承，而type不能。
* interface可以被合并（merge），而type不行。
* interface支持extends关键字，可以继承其他接口，而type不支持extends关键字。

在大多数情况下，interface和type都可以用来定义数据结构，选择使用哪一个主要取决于个人的偏好和具体的使用场景。

## 请说说你对TypeScript中泛型的理解

泛型是一种在TypeScript编程中使用的机制，它可以增强代码的可复用性和类型安全性。

泛型允许在定义函数、接口或类时使用一个占位符作为类型参数，以便在使用时可以动态地指定具体的类型。通过使用泛型，可以在不同类型上执行相同的操作，而无需为每种类型编写重复的代码。

通过使用泛型，可以在编译时捕获类型错误，提早发现和解决问题，从而增强代码的可靠性。在编译时，TypeScript会根据泛型的约束条件检查类型参数是否符合预期，如果不符合则会发出编译错误。

泛型还可以与接口、类、函数等结合使用，可以创建泛型函数、泛型类和泛型接口来处理各种数据类型的输入和输出，从而提供更灵活和通用的代码。通过使用泛型，可以提高代码的可读性、可维护性和可扩展性。

## 请说说你对TypeScript中模块的理解

TypeScript 中的模块，是用来组织代码的一种方式，可以将相关的代码、类型定义、变量和函数封装在一个独立的单元中。它提供了明确的导入和导出语法，以便在不同的模块之间共享和复用代码。

通过使用模块，可以将代码分割成不同的文件，从而提高代码的可维护性和可复用性。模块可以包含类型定义、接口、类、变量和函数等，可以使用 import 关键字导入模块中定义的内容，并使用 export 关键字将模块中的内容导出。

模块还支持命名空间（namespace）的概念，可以用于将相关的代码分组，并避免命名冲突。命名空间可以嵌套使用，使代码结构更加清晰。

在模块中使用可以提供良好的封装和隔离性，使得模块之间的代码逻辑更加清晰明了。通过使用模块，可以将复杂的系统拆分成更小的模块，便于团队协作和扩展。

总之，TypeScript 中的模块提供了一种组织和管理代码的机制，使得代码更加模块化、可维护和可复用。

## 请说说TypeScript有哪些特性？

TypeScript是JavaScript的一个超集，它扩展了JavaScript的功能并添加了静态类型。它具有以下特性：

1. 静态类型：TypeScript能够在编译时检查类型错误，提高代码质量和可靠性。
2. 类型注解：可以为变量、函数参数和返回值等添加类型注解，明确数据类型，提高代码可读性。
3. 类型推断：TypeScript能够根据上下文推断变量的类型，减少手动标注类型的工作量。
4. 类型系统：TypeScript提供了丰富的类型系统，包括基本类型、枚举类型、联合类型、交叉类型等，能够更精确地描述数据类型。
5. 类型检查：TypeScript具有强大的类型检查能力，可以在编译时发现潜在的类型错误，并提供相应的错误提示和修复建议。
6. ES6+支持：TypeScript支持并扩展了ECMAScript 6及以上版本的语法特性，包括箭头函数、解构赋值、类、模块化等等。
7. IDE支持：TypeScript能够与大多数主流的IDE集成，提供代码补全、自动重构、错误提示等丰富的开发辅助功能。
8. 可选的静态类型：TypeScript允许将变量、函数参数等定义为可选的静态类型，提高代码的灵活性和可组合性。

总之，TypeScript是一种强大而灵活的编程语言，通过引入静态类型和增强JavaScript的功能，能够提高代码质量、可读性和可维护性。

## 请说下TypeScript中的类型断言是什么？

在TypeScript中，类型断言是一种告诉编译器一个值的类型的方法。它可以用来覆盖编译器对值类型的推断，从而使开发者自己确定值的类型。类型断言有两种形式：

1. 尖括号语法

```typescript
let value: any = "Hello TypeScript!";
let strLength: number = (<string>value).length;
```

2. as语法

```typescript
let value: any = "Hello TypeScript!";
let strLength: number = (value as string).length;
```

尽管两种形式的类型断言是等效的，但是尖括号语法不能在JSX中使用，而as语法可以在任何类型的断言中使用。不管使用哪种形式的类型断言，都应该注意确保值的实际类型与断言类型相一致，否则在运行时可能会导致错误。

## 请说下在TypeScript中命名空间与模块的区别？

在TypeScript中，命名空间和模块都是用于组织代码的概念，但它们有一些不同之处。

命名空间是在全局范围内定义一组相关的类型、函数和常量。它们可以通过嵌套来创建层次结构，并使用`namespace`关键字来定义。命名空间可以通过`export`关键字来导出其成员，并使用`import`关键字来导入其他命名空间或模块。

模块是在模块范围内定义独立的功能单元。一个模块可以包含一个或多个命名空间，它通过`export`关键字来导出其成员，其他模块则可以使用`import`关键字来导入这些成员。模块通常被编译为独立的文件，并使用ES6的模块系统进行加载。

总的来说，命名空间用于在全局范围内组织代码，而模块则用于在模块范围内组织代码，并可被导入和导出。在较新的TypeScript版本中，推荐使用模块来组织代码，因为模块具有更好的语义化和类型检查支持。

## TypeScript中支持的访问修饰符有哪些？

TypeScript中支持的访问修饰符有public、private和protected。

* public：公共访问修饰符，可以在类的内部和外部访问。
* private：私有访问修饰符，只能在类的内部访问。
* protected：受保护访问修饰符，可以在类的内部以及其子类中访问。

## 请说说你对TypeScript中枚举的理解

在 TypeScript 中，枚举（enum）是一种数据类型，用于定义一组命名的常量。

枚举通过将一组具有唯一名称和值的元素封装在一个命名空间中来定义。每个元素都可以通过名称或值进行访问，这使得代码更加具有可读性和可维护性。

枚举可以帮助开发人员避免使用魔术常量，提供了一种更好的方式来表示一组相关的常量。例如，可以使用枚举来表示颜色、方向、状态等一组有限的选项。

在 TypeScript 中，枚举的定义类似于常量的定义，但可以指定各个元素的值。例如：

```typescript
enum Color {
  Red,
  Green,
  Blue,
}
```

在上面的示例中，Color 是一个枚举类型，包含了三个元素：Red、Green 和 Blue。默认情况下，它们的值分别为 0、1 和 2。可以通过名称或值来访问枚举元素：

```typescript
let redColor: Color = Color.Red;
let greenColor: Color = 1;
console.log(Color[redColor]); // 输出 "Red"
console.log(Color[greenColor]); // 输出 "Green"
```

枚举还可以指定元素的值，可以是数字、字符串甚至是表达式。例如：

```typescript
enum Direction {
  Up = 1,
  Down = Up * 2,
  Left = 5,
  Right = Left + 1,
}
```

在上面的示例中，Up 的值为 1，Down 的值为 2，Left 的值为 5，Right 的值为 6。

总结一下，枚举是 TypeScript 中用于定义一组命名的常量的数据类型，通过枚举可以提高代码的可读性和可维护性。

## TypeScript中的Declare关键字有什么作用？

在TypeScript中，`declare`关键字用于声明一个全局变量、全局类型或全局命名空间，它告诉编译器该符号已经存在，不需要编译器进行检查或转换。

`declare`关键字主要有以下用途：

1. 声明全局变量：使用`declare`关键字可以在TypeScript中声明一个全局变量，而不需要先定义它的类型或初始值。这样，在编译时，TypeScript编译器就不会对该变量进行类型检查。

```typescript
declare var MyGlobalVariable: number;
```

2. 声明全局类型：使用`declare`关键字可以在TypeScript中声明一个全局类型，并且告诉编译器该类型已经存在。这样，在使用该类型时，编译器就不会报错。

```typescript
declare interface MyInterface {
  // ...
}
```

3. 声明全局命名空间：使用`declare`关键字可以在TypeScript中声明一个全局命名空间，以在模块之间共享接口、类型和变量。通过声明命名空间，可以在不同文件中访问和扩展命名空间中的内容。

```typescript
declare namespace MyNamespace {
  // ...
}
```

需要注意的是，使用`declare`关键字声明的全局变量、全局类型或全局命名空间，实际上并没有在编译后的JavaScript代码中生成对应的声明，它们只是用于提供类型检查和提示的辅助信息。在实际运行时，这些声明需要通过其他方式来引入或实现。

### 实现一个ts内置类型

实现一个ts内置类型需要遵循以下步骤：

1. 确定需要实现的内置类型的名称和功能。
2. 创建一个新的ts类型，并定义它的接口或类型别名。
3. 给新类型添加必要的方法和属性。
4. 测试新定义的类型，确保它符合预期。

例如，我们来实现一个名为`NonEmptyArray`的类型，这个类型可以确保数组不为空。

```typescript
type NonEmptyArray<T> = [T, ...T[]];

const arr1: NonEmptyArray<number> = [1];
const arr2: NonEmptyArray<string> = ['foo', 'bar'];
const arr3: NonEmptyArray<{}> = [{}, { foo: 'bar' }];

// 错误示例：数组为空
const arr4: NonEmptyArray<any> = []; // 报错：Type '[]' is not assignable to type 'NonEmptyArray<any>'
```

在上面的示例中，我们使用了Rest参数的语法来确保数组中包含至少一个元素。这样，当我们尝试定义一个空数组时，ts会抛出一个类型错误。

### ts泛型的使用

在 TypeScript 中，泛型（Generics）是一种特殊的类型，可以用来创建可以适用于多种类型的可重用代码。

使用泛型可以使得代码更加通用和灵活，并且可以避免代码重复和类型错误。常见的使用场景包括：数组、函数和类。

1. 数组的泛型

使用泛型可以在数组中存储任意类型的数据，例如：

```ts
let arr1: Array<number> = [1, 2, 3]; // 存储数字类型
let arr2: Array<string> = ['hello', 'world']; // 存储字符串类型
let arr3: Array<boolean> = [true, false]; // 存储布尔类型
```

2. 函数的泛型

使用泛型可以使函数可以适用于不同类型的参数和返回值，例如：

```ts
function identity<T>(arg: T): T {
    return arg;
}

let output1 = identity<string>('hello world'); // 返回字符串类型
let output2 = identity<number>(100); // 返回数字类型
```

在这个例子中，`identity` 函数使用 `T` 类型参数，这个参数可以在函数体内任意使用。在调用 `identity` 函数时，可以使用任意类型的参数来作为 `T` 类型的值。

3. 类的泛型

使用泛型可以使类可以适用于不同类型的属性和方法，例如：

```ts
class GenericClass<T> {
    private value: T;

    constructor(value: T) {
        this.value = value;
    }

    getValue(): T {
        return this.value;
    }

    setValue(value: T) {
        this.value = value;
    }
}

let obj1 = new GenericClass<string>('hello'); // 存储字符串类型
let obj2 = new GenericClass<number>(100); // 存储数字类型

console.log(obj1.getValue()); // 输出 hello
console.log(obj2.getValue()); // 输出 100
```

在这个例子中，泛型参数 `T` 可以用于类的属性和方法中。在创建实例时，可以使用不同类型的参数来作为 `T` 类型的值。在调用实例的 `getValue` 方法时，返回的值也会根据不同类型的参数而变化。

***

END

[![知识共享许可协议](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
