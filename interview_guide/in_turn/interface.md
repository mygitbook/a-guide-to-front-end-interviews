# 后端接口

\[toc]

## API是什么？

API全称 Application Programming Interface，应用程序接口

**API就是可以轻松实现和其他软件的交互。**

## SDK是什么？

SDK全称software development kit，软件开发工具包

**就是第三方服务商提供的实现产品软件某项功能的工具包。**

_sdk包含了多个api_\*

## API和SDK的区别？

* API是一个函数，有其特定的功能；而SDK是一个很多功能函数的集合体，一个工具包。
* API是数据接口，SDK相当于开发集成工具环境，要在SDK的环境下来调用API。
* API接口对接过程中需要的环境需要自己提供，SDK不仅提供，还提供很多API。
* 简单功能调用，API调用方便快捷；复杂功能调用，SDK功能齐全。
